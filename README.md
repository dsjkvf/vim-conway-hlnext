# Highlighting the current search match

## About

This is a Vim plugin for highlighting search matches. Originally written by [Damian Conway](http://damian.conway.org/About_us/Bio_formal.html), copied now from [Damian's collection](https://github.com/thoughtstream/Damian-Conway-s-Vim-Setup) of his Vim-related files to a separate repository in order to make the installation process easier.

## Appendix

As an alternative, one may be also interested in [SearchParty](https://github.com/dahu/SearchParty) or [vim-oblique](https://github.com/junegunn/vim-oblique) plugins.
